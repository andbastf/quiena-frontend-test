export class Movie {
    Id: number;
    Title: string;
    Year: number;
    Runtime: string;
    Plot: string;
    Poster: string;
    Rating: number;
    Price: string;
}
