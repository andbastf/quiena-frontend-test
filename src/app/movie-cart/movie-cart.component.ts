import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { CartService } from '../services/cart.service';
import { Movie } from '../models/movie';


@Component({
  selector: 'app-movie-cart',
  templateUrl: './movie-cart.component.html',
  styleUrls: ['./movie-cart.component.css']
})
export class MovieCartComponent implements OnInit {
  cart: Movie[];

  constructor(private cartService : CartService) { }

  ngOnInit(): void {
    this.getCart();
  }

  /*Metodo para obtener los valores del carrito de compras*/
  getCart(): void{
    this.cart = this.cartService.getCart();
  }

  /*Metodo para obtener el total de las peliculas */
  getTotal(): number{   
    let total: number = 0;
     this.cart.forEach( (movie: Movie) => {
       total += parseFloat(movie.Price);
     })

    return total;
  }

  /**
    Metodo para borrar del carrito de compras
    @param: pelicula a eliminar 
  */
  removeFromCart(movie): void{
    this.cart = this.cart.filter( m => m != movie);
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

}
