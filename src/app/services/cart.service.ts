import { Injectable, OnInit } from '@angular/core';
import { Movie } from '../models/movie';

@Injectable({
  providedIn: 'root'
})
export class CartService implements OnInit{

  cart: Movie[] = [];

  constructor() { 
    this.ngOnInit();
  }

  ngOnInit(): void {
    if(!localStorage.getItem("cart")){
      localStorage.setItem("cart", "[]")  
    }
  }
  
  setCart(movie: Movie): void{
    this.cart = JSON.parse(localStorage.getItem("cart"));
    this.cart.push(movie);
    localStorage.setItem("cart", JSON.stringify(this.cart));
  }

  getCart() : Movie[]{
    return JSON.parse(localStorage.getItem("cart"));
  }
}
