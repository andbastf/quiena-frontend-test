import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'; 
import { Movie } from '../models/movie';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(protected http: HttpClient) { }

  /*Metodo para llamar al endpoint de peliculas*/
  getMovieInfo(): Observable<Movie[]>{
    return this.http.get<Movie[]>("http://localhost:4200/assets/data.json");
  }



}
