import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { CartService } from '../services/cart.service';
import { Movie } from '../models/movie';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  movies: Movie[] = [];
  successMessage: boolean = false;

  constructor(private restService : RestService, private cartService : CartService) { }

  ngOnInit(): void {
    this.loadMovies();
  }

  loadMovies() : void{
    this.restService.getMovieInfo().subscribe(
      data => {
        let cart = this.cartService.getCart()        
        data = data.filter( m =>{
          let result = cart.find(c => { return m.Id == c.Id})
          return !result;
        });
        this.movies = data;
      },
      error =>{
        console.log(error);
      }
    );

  }
  /**
   * Metodo para agregar pelicula
   * @param movie Pelicula a agregar
   */
  addToCart(movie): void{
    this.cartService.setCart(movie);
    this.movies = this.movies.filter(m => m != movie);
    this.showAlert()
    }

  
  showAlert(): void{
    setTimeout(() => this.successMessage = false, 7500);
    this.successMessage = true;
  }
  

  /**
   * Metodo para ordenar las peliculas
   * @param value dato que se quiere ordenar
   */
  sortMovies(value : string){
    this.movies.sort((a, b) => (a[value] > b[value]) ? 1 : -1);
  }


}
